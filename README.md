# Table Alternate Rows

Table Alternate Rows is a text format filter that inserts even and odd classes
into user submitted tables within nodes. This allows for proper table theming to
occur while allowing users to use regular table tags or a WYSIWYG editor to
create tables.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/table_altrow).

 * To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/table_altrow).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Enable the filter for formats on the `/admin/config/content/formats` page.


## Maintainers

- Andrew Berry - [deviantintegral](https://www.drupal.org/u/deviantintegral)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
